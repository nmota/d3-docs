# CAPÍTULO I
(Associação)

## Artigo 1º
(Objetivos)

A D3 tem por Objectivos: • [TODO: listar explicitamente objectivos: eliminar DRM, etc etc]

## Artigo 2º
(Revisão dos Estatutos e Regulamento)

1. O presente Regulamento e Estatutos da Associação são passíveis de revisão por proposta apresentada pela Direcção ou por um terço dos associados.
2. As propostas deverão ser enviadas por escrito e com a lista do associados que a subescrevem à Mesa da Assembleia Geral.
3. A Mesa da Assembleia Geral deverá colocar na ordem de trabalhos da primeira Assembleia Geral subsequente a votação das propostas de revisão dos Estatutos.
4. As propostas deverão ser publicadas na página oficial da Associação em conjunto com respectivo anúncio da Assembleia Geral, definido no n.3 do artigo 11º.
5. As propostas de revisão dos Estatutos serão aceites se reunirem o voto favorável de pelo menos dois terços dos associados presentes na Assembleia Geral, com um quórum de pelo menos três quartos dos associados.

## CAPÍTULO II (Sócios)

### Artigo 3º
 (Admissão)

1. A admissão dos associados depende cumulativamente de:

  a) Preenchimento correcto do Formulário de Candidatura publicado na página oficial da Associação;

  b) Aprovação pela Direcção;

  c) Pagamento das Quotas relativas ao primeiro ano, num prazo de 90 dias após a sua aprovação pela Direcção.

2. Após recepção e análise do Formulário de Candidatura, deve a Direcção comunicar ao candidato a sua aceitação ou rejeição.

3. A deliberação da Direcção sobre a Candidatura de admissão é susceptível de recurso para a primeira Assembleia Geral subsequente ao recurso.
4. Poderão recorrer da decisão da Direção os sócios da Associação e o candidato, podendo este assistir à Assembleia Geral e participar na discussão deste ponto da ordem de trabalhos, sem direito a voto.
5. O sócio que seja admitido compromete-se a comunicar à Direcção qualquer alteração nos dados constantes do Formulário de Candidatura.

### Artigo 4º
(Direitos e Deveres)

1. São direitos de cada associado da D3:

  a) Eleger e ser eleito para os órgãos associativos;

  b) Apresentar à Direcção propostas, críticas ou sugestões que julgarem convenientes;

  c) Participar e ser informado acerca de todas as actividades da Associação;

  d) Participar nas Assembleia Gerais e apresentar propostas, moções e requerimentos;

  e) Votar na Assembleia Geral. Cada associado, individual ou colectivo, tem direito a um voto por votação;

  f) Recorrer para a Assembleia Geral de decisões tomadas pela Direcção;

  g) Requerer a convocação de Assembleias Gerais nos termos do n.º2 do artigo 11º dos presentes Estatutos;

  h) Participar em grupos de trabalho ou comissões criadas pela Direcção e pelos grupos de trabalho da D3;

  i) Beneficiar de todos os recursos postos ao serviço da Associação, em particular estar inscrito e participar no fórum de discussão interno dos associados da D3.

2. São deveres de todos os associados da D3:

  a) Cumprir as disposições dos Estatutos e dos Regulamentos internos, bem como acatar as deliberações validamente emitidas pelos Órgãos Sociais;

  b) Exercer com zelo, dedicação e eficiência os cargos para que forem eleitos;

  c) Colaborar com todas as iniciativas que concorram para o prestígio e desenvolvimento da D3;

  d) Pagar atempadamente as quotas que vierem a ser fixadas nos termos definidos no Regulamento Interno.

### Artigo 5º
(Expulsão)

1. A expulsão de um sócio é decidida em Assembleia Geral por maioria de dois terços.
2. A expulsão de um sócio terá de ser fundada em violação grave e culposa dos Estatutos, deste Regulamento Interno ou de manifesta conduta imprópria perante outro associado.
3. A exclusão terá de ser precedida de processo escrito, do qual constem a indicação das infracções, a sua qualificação, a prova produzida, a defesa do sócio e a proposta de aplicação da medida de exclusão.
4. A proposta de expulsão a exarar no processo será fundamentada e notificada por escrito ao sócio, com a antecedência de, pelo menos, quinze dias, em relação à data da Assembleia Geral que sobre ela deliberará.

### Artigo 6º
(Suspensão)

1. Cabe à Direção decidir a aplicar a suspensão de sócios.
2. Para a suspensão, aplicam-se, com as devidas adaptações, os preceitos dos pontos 2, 3 e 4 do artigo anterior, excepto quando aplicavel o nº1 do artigo 7º, em que se prescinde do processo referido no artigo anterior.
3. A decisão de suspensão será fundamentada e notificada por escrito ao sócio, contendo obrigatoriamente a indicação da duração da pena de suspensão.
4. A decisão de suspensão é susceptível de recurso para a primeira Assembleia Geral subsequente ao recurso.
5. As penas de suspensão aplicadas pela Direcção devem ser imediatamente comunicadas ao Conselho Fiscal anexando cópia de todo o processo.

### Artigo 7º
(Auto-suspensão e exclusão)

1. Um sócio pode solicitar à Direcção a sua suspensão ou exclusão, mediante requerimento escrito devidamente identificado.
2. O tempo de quotização restante no momento do pedido de suspensão será reposto na altura que o sócio solicitar o cancelamento da suspensão.
3. O tempo de quotização restante no momento do pedido de exclusão não será devolvido ao sócio.
4. A Direcção deverá notificar imediatamente ao Conselho Fiscal da suspensão ou exclusão.

### Artigo 8º
(Quotas)

1. A estrutura de Quotas anuais será decidida pela Assembleia Geral por proposta da Direcção em exercício.
2. Os Sócios deverão regularizar as quotas no máximo até 30 dias após a caducidade da quotização anterior.
3. A Direcção poderá suspender um sócio que, após decorridos os 60 dias, não tenha regularizado a situação;
4. Se a irregularidade se mantiver por mais de 120 dias poderá a Direcção excluir o sócio, devendo comunicar-lhe por escrito esta decisão e notificar o Conselho Fiscal.

## CAPÍTULO III
(Órgãos Sociais)

### Artigo 9º
(Reuniões)

1. As deliberações dos Órgãos são tomadas por maioria absoluta com a presença de mais de metade dos membros, exceto quando especificamente indicado em contrário nos Estatutos ou neste Regulamento Interno;
2. Será sempre lavrada acta das reuniões de qualquer Órgão, a qual é obrigatoriamente assinada pelos presentes e disponibilizada na página oficial da Associação;
3. As reuniões dos Órgãos poderão ocorrer de forma presencial ou remota, sendo que no caso da Assembleia Geral os elementos remotos não poderão ser mais do que um terço do total dos elementos na reunião.

### Artigo 10º
(Direcção)

A Direcção reúne quando achar conveniente para prossecução dos seus objetivos, quer presencialmente ou via eletrónica.

### Artigo 11º
(Assembleia Geral)

1. A Assembleia Geral reúne ordinariamente, pelo menos uma vez por ano, para aprovação do relatório de actividades e contas e para eleger a Direcção e o Conselho Fiscal.
2. A Assembleia Geral reúne extraordinariamente por iniciativa da Direcção ou do Conselho Fiscal, ou por requerimento um terço dos sócios.
3. A Assembleia Geral será convocada pela Direcção, até 60 dias antes da sua data, através de anúncio publicado na sede social, na página oficial e por correio eletrónico.
4. A Assembleia Geral iniciará os seus trabalhos à hora marcada, desde que estejam presentes metade dos associados inscritos na Associação, podendo funcionar meia hora mais tarde qualquer que seja o número de associados presentes.
5. Compete à Mesa da Assembleia Geral tratar dos aspectos necessários à realização da Assembleia Geral, coordenar e dirigir a mesma e redigir e assinar as respectivas actas.
6. A deliberação relativa à dissolução da Associação requer o voto favorável de pelo menos três quartos do número de todos os associados.

### Artigo 12º
(Conselho Fiscal)

1. O Conselho Fiscal reúne ordinariamente, pelo menos uma vez por ano, para aprovar o relatório de contas e relatório de atividades;
2. O Conselho fiscal reúne extraordinariamente para deliberar sobre assuntos da sua competência, definidos no presente Regulamento e nos Estatutos.

### Artigo 13º
(Transparência)

1. A Associação rege-se pelo princípio da total transparência das fontes e modos de financiamento.
2. Os relatórios e contas e o relatório de actividades da Associação são públicos e devem estar disponíveis na página oficial da Associação.

## CAPÍTULO IV
(Processo Eleitoral)

### Artigo 14º
(Eleições)

1. Os membros da Mesa da Assembleia Geral, os membros da Direcção e os membros do Conselho Fiscal, são eleitos bienalmente por escrutínio secreto.
2. A data das eleições para a eleição dos membros dos Órgãos deverá ser fixada dois anos após a eleição anterior com uma tolerância de 15 dias antes ou depois.
3. O Órgão competente deverá comunicar a todos os sócios, por correio electrónico e colocando um aviso na página oficial da Associação, com pelo menos 90 dias de antecedência, a data marcada para as eleições.
4. As candidaturas às eleições deverão ser organizadas com base em listas de candidatos, apresentadas e aceites nos termos do presente Regulamento.

### Artigo 15º
(Preparação e fiscalização do acto eleitoral)

1. Os actos preparatórios e a orientação, fiscalização e direcção do acto eleitoral competem à Comissão Eleitoral, que será composta por um Presidente e dois vogais, definidos de acordo com a competências do Órgão que a constituí.
2. A Comissão eleitoral é constituída pelos membros da Mesa da Assembleia Geral.
3. Em caso de impedimento da Mesa da Assembleia Geral as competências definidas no n.º1 passam para o Conselho Fiscal ou em alternativa para o Presidente da Direção e dois membros da Direção à sua escolha.
4. À Comissão Eleitoral serão agregados os vogais verificadores, a que se refere o n.º 1 do artigo 17º.
5. Na falta de elementos da Comissão Eleitoral, o Presidente da comissão escolherá de entre os associados, aquele ou aqueles que forem necessários para constituir a respectiva comissão.

### Artigo 16º
(Cadernos Eleitorais)

1. No dia seguinte ao anúncio referido no ponto 3 do artigo 11º, será afixada na página oficial da Associação, a lista dos sócios efectivos no pleno gozo dos seus direitos sociais, com indicação dos cargos que exercem, quer nos órgãos sociais, quer em outras estruturas orgânicas da Associação.
2. Qualquer associado poderá reclamar, por escrito, da inclusão ou omissão do associado nas listas referidas no número anterior, até 15 dias antes da data designada para a Assembleia Geral.
3. As reclamações serão apreciadas pela Comissão Eleitoral, nas quarenta e oito horas seguintes ao termo dos prazos fixados no número anterior, sendo dado conhecimento por escrito da decisão ao sócio ou sócios reclamantes.
4. A relação dos sócios efectivos, depois da rectificação em função da procedência ou improcedência de eventuais reclamações, constituirá o Caderno Eleitoral e estará afixado no local da realização da Assembleia Geral e durante toda a realização do respectivo acto.

### Artigo 17º
(Candidaturas)

1. Na apresentação das candidaturas, os proponentes deverão indicar qual de entre eles exercerá as funções de vogal verificador e fará parte da Comissão Eleitoral como seu representante, bem como o respectivo suplente.
2. A apresentação das candidaturas será feita ao Presidente da Comissão Eleitoral por escrito, até 30 dias antes da data para a qual tiver sido convocado o acto eleitoral.
3. No dia imediato, deverá a Comissão Eleitoral, reunida com os vogais verificadores, comprovar a conformidade das candidaturas com os Estatutos e o presente Regulamento.
4. Se for detectada alguma irregularidade, o vogal verificador representante da respectiva candidatura disporá das 48 horas seguintes para a sua correcção, sob pena da mesma não poder ser considerada.
5. Não havendo candidaturas válidas para todos ou alguns dos órgãos ou cargos elegendos, o Presidente da Comissão Eleitoral notificará a Direcção em exercício, que fica obrigada a propôr as candidaturas em falta no prazo de 48 horas.
6. Das decisões da Comissão Eleitoral, que serão tomadas por maioria simples, cabendo a cada membro um voto e ao Presidente voto de qualidade, cabe recurso para a Assembleia Geral, que será apreciado como ponto prévio à realização do acto eleitoral.

### Artigo 18º
(Relação das candidaturas: boletins de voto)

1. O Presidente da Comissão Eleitoral promoverá a afixação na sede social e na página oficial da Associação, depois de assinada pela Comissão Eleitoral, a relação das candidaturas aceites, em conformidade com as quais serão elaborados os boletins de voto, 15 dias antes da data para a qual tiver sido convocado o acto eleitoral.
2. As candidaturas serão diferenciadas por letras, correspondendo a ordem alfabética à ordem cronológica da respectiva apresentação.
3. A partir das listas definitivas os serviços da Associação providenciarão boletins de voto, que serão postos à disposição no local em que se realizar o acto eleitoral, e que serão de aspecto absolutamente igual para todas as listas.
4. Os processos das candidaturas ficarão arquivados na sede da Associação e deles constarão todos os documentos respeitantes a cada candidatura, e entre eles as actas das reuniões da Comissão Eleitoral.

### Artigo 19º
(Votação)

A votação será por escrutínio secreto e decorrerá no local referido na convocatória, segundo o horário nela indicado, só podendo votar os sócios constantes do caderno eleitoral a que se refere o artigo 16º.

### Artigo 20º
(Proclamação das listas mais votadas)

1. A proclamação das listas mais votadas no escrutínio será feita logo após o apuramento ser comunicado a todos os sócios.
2. Se, para cada órgão social, nenhuma das listas alcançar a maioria absoluta de votos expressos, a acto eleitoral será repetido 2 horas mais tarde, concorrendo apenas as duas listas mais votadas.

### Artigo 21º
(Conclusão dos trabalhos: reclamações)

1. Findos os trabalhos, a Mesa da Assembleia Eleitoral redigirá a respectiva acta, que será assinada por todos os seus membros.
2. Quaisquer reclamações sobre o acto eleitoral deverão ser presentes à Mesa da Assembleia Eleitoral, nas 48 horas seguintes, a qual decidirá nas 24 horas seguintes, comunicando por escrito a sua decisão aos reclamantes.
3. Da decisão tomada nos termos do número anterior, cabe recurso aos tribunais.

## CAPÍTULO V
(Grupos de Trabalho)

### Artigo 22º
(Fins e Criação)

1. Para melhor levar a cabo as actividades a que se propõe, pode a Direcção designar Grupos de Trabalho diferenciados.
2. Os Grupos de Trabalho têm por fim a intervenção nas respectivas áreas de actividade, estabelecidas quando da criação de cada Grupo de Trabalho e definição do respectivo âmbito.

### Artigo 23º
(Competências)

Compete aos Grupos de Trabalho:

1. Levar a cabo as actividades que se enquadrem no seu âmbito;

2. Dinamizar a intervenção dos respectivos membros na vida associativa;
3. Propor à Direcção a tomada de posições internas à Associação ou públicas sobre matérias do respectivo âmbito de actividades.

### Artigo 24º
(Composição)

1. Os Grupos de Trabalho são compostos por todos os associados interessados nas respectivas actividades ou que às mesmas queiram dar o seu contributo pessoal.
2. Os Grupos de Trabalho podem integrar ainda elementos não-associados, sempre que a sua participação se justifique.

### Artigo 25º
(Coordenação)

1. A Direcção deverá estar representada, por um seu elemento, em cada um dos Grupos de Trabalho.
2. Este elemento fará a ponte entre o Grupo de Trabalho e a Direcção, mantendo-a informada das actividades do grupo.
