## CAPÍTULO I – DISPOSIÇÕES GERAIS

### Artigo 1.º – Denominação, sede e duração

A associação D3 - Defesa dos Direitos Digitais, adiante designada por D3, é uma associação de direito privado sem fins lucrativos e de âmbito nacional, tem a sede em [TODO], e constitui-se por tempo indeterminado.

### Artigo 2.º – Visão

A D3 tem por Visão um Portugal em que:
•    A Internet é reconhecida como direito básico fundamental e o acesso a esta é garantido a todos os cidadãos:
•    Os meios de comunicação, digitais ou de outro carácter, não estão sujeitos a monitorização nem a censura ou quaisquer outros tipos de restrições, permitindo o livre acesso a informação, conhecimento e cultura.
•    Os cidadãos têm total controlo sobre o armazenamento e a divulgação da sua informação pessoal.
•    Os cidadãos têm total autonomia e liberdade de escolha a respeito do software instalado ou executado nos seus dispositivos digitais.
•    Os cidadãos têm acesso à informação recolhida e produzida pelo Estado ou por ele financiada, em formatos abertos e de forma atempada.
•    TODO: transparência

### Artigo 3.º – Missão

A D3 tem por Missão a defesa dos direitos e liberdades fundamentais dos cidadãos no contexto digital, nomeadamente assegurando a sua liberdade de escolha e autonomia, a sua privacidade e o livre acesso à informação, conhecimento e cultura, entendendo a defesa dos meios digitais como potenciadora dos mecanismos de autocontrolo do processo democrático.

### Artigo 4º – Da organização

1. A D3 terá um Regulamento Interno que, aprovado em Assembleia Geral, disciplinará o seu funcionamento.
2. A D3 prosseguirá os seus fins e desenvolverá as suas actividades num contexto de total independência perante qualquer outra entidade, seja ela de cariz público ou privado.
3. A fim de cumprir os seus objectivos, a D3 irá organizar-se em tantos grupos de trabalho quantos sejam necessários, os quais se regerão pelas disposições estatutárias e regimentais.
4. O património da D3 será constituído pelas quotizações e donativos dos seus associados e de outras entidades e pelos bens e verbas adquiridos no exercício das suas actividades.


## CAPÍTULO II – ASSOCIADOS

### Artigo 5.º – Dos associados

1. A D3 é constituída por número ilimitado de associados.
2. Podem solicitar a adesão à D3, por requerimento dirigido à Direcção, todos os cidadãos ou colectividades que se identifiquem com as finalidades da associação, mediante inscrição e pagamento das quotas conforme definido em Regulamento Interno.
3. Um associado pode, nos termos do Regulamento Interno da D3, sofrer pena de suspensão ou expulsão.

## CAPÍTULO III – ÓRGÃOS SOCIAIS

### Artigo 6.º – Dos Órgãos Sociais

1. São Órgãos Sociais da D3 a Assembleia Geral, a Direcção e o Conselho Fiscal.
2. Os membros da Direcção e do Conselho Fiscal serão eleitos, em Assembleia Geral, individualmente de entre os associados, em mandatos bianuais.

### Artigo 7.º – Assembleia Geral

1. A Assembleia Geral é o órgão soberano da D3 e é constituída por todos os associados no pleno gozo dos seus direitos associativos.
2. Os associados colectivos deverão mandatar expressamente, para cada Assembleia Geral, o seu representante.
3. A Mesa da Assembleia Geral será constituida por um presidente e dois vogais eleitos de entre os associados presentes, competindo-lhes dirigir as reuniões da Assembleia Geral e lavrar as respectivas actas.
4. A competência da Assembleia Geral e a forma do seu funcionamento são os estabelecidos no Código Civil, designadamente no artigo 170º, e nos artigos 172º a 179º, assim como no Artigo 11.º abaixo descrito.

### Artigo 8.º – Competências da Assembleia Geral

São competências da Assembleia Geral:

1. Eleger e destituir os titulares dos Órgãos Sociais;
2. Discutir e aprovar o relatório de actividade referente ao ano anterior apresentado pela Direcção;
3. Discutir e aprovar o relatório de contas referente ao ano anterior apresentado pela Direcção, atento o parecer do Conselho Fiscal;
4. Proceder à revisão dos Estatutos e votar os regulamentos;
5. Deliberar em segunda instância sobre as decisões de adesão ou recusa de adesão de novos associados;
6. Deliberar a dissolução e liquidação da D3;
7. Autorizar a associação a demandar os titulares dos cargos de Direcção por actos praticados no exercício dos mesmos;
8. Deliberar sobre quaisquer outras questões não compreendidas na competência exclusiva dos outros órgãos.

### Artigo 9.º – Direcção

A Direcção é composta por número ímpar de associados não inferior a 5, conforme deliberação da Assembleia Geral, sendo permitida a sua reeleição.

### Artigo 10º – Competências da Direcção

São competências da Direcção:

1. Cumprir e fazer cumprir os presentes Estatutos;
2. A gestão executiva, social, administrativa e financeira da D3;
3. Coordenar e orientar o trabalho da D3, promovendo, pelas formas que entender, mas com subordinação às linhas gerais decididas pela Assembleia Geral, o cumprimento dos objectivos definidos nos regulamentos;
4. Deliberar sobre a adesão, filiação e/ou desfiliação da D3 em organizações nacionais e internacionais;
5. Dar cumprimento às deliberações da Assembleia Geral e tomar posições em nome da D3 entre Assembleias Gerais;
6. Gerir a contabilidade, elaborando e apresentando os relatórios de actividades e contas ao Conselho Fiscal, até um mês antes do final do seu mandato;
7. Representar a D3 em juízo e fora dele, obrigando-se pela assinatura de três dos seus membros.

### Artigo 11.º – Conselho Fiscal

O Conselho Fiscal, eleito em Assembleia Geral, é composto por três associados não integrantes da Direcção.

### Artigo 12 – Competências do Conselho Fiscal

Ao Conselho Fiscal compete:
1.  Fiscalizar os actos administrativos e financeiros da Direcção;
2.  Fiscalizar as contas e relatórios da D3 dando parecer fundamentado sobre os relatórios de actividades e contas fornecidos pela Direcção.
3.  Velar pelo cumprimento dos presentes Estatutos.
4.  A forma do seu funcionamento é a estabelecida no artigo 171º do Código Civil.

## CAPÍTULO IV – DISPOSIÇÕES FINAIS

### Artigo 13º – Da revisão dos estatutos

Os presentes Estatutos são passíveis de revisão de acordo com o estipulado no Regulamento Interno.

### Artigo 14.º – Extinção

A D3 extingue-se por deliberação dos seus membros ou nos termos da lei, competindo à Assembleia Geral eleger uma comissão liquidatária e decidir sobre o destino do seu património, devendo o mesmo, na falta de deliberação, ser atribuído a entidade com fim similar, nos termos e sem prejuízo das limitações previstas na legislação em vigor.
